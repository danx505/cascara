import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  faAngleDown,
  faAngleUp,
  faCaretRight,
  faCircleXmark,
  faDiagramProject,
  faFileCircleQuestion,
  faMinus,
  faPenToSquare,
  faPencil,
  faPlus,
  faRightToBracket,
  faSearch,
  faSliders,
} from '@fortawesome/free-solid-svg-icons';
import AccountLoginForm from '@/components/AccountLoginForm.vue';
import App from './App.vue';
import CasAlert from '@/components/SiteAlert.vue';
import CasFooter from '@/components/SiteFooter.vue';
import CasHeader from '@/components/SiteHeader.vue';
import CategoryForm from '@/components/CategoryForm.vue';
import CategoryQuirkForm from '@/components/CategoryQuirkForm.vue';
import ExecutionsListGroupItem from '@/components/ExecutionsListGroupItem.vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import GroupForm from '@/components/GroupForm.vue';
import TasksListGroupItem from '@/components/TasksListGroupItem.vue';
import UsersDashboardListItem from '@/components/UsersDashboardListItem.vue';
import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import { createORM } from 'pinia-orm';
import { createPinia } from 'pinia';
import { library } from '@fortawesome/fontawesome-svg-core';
import messages from '@intlify/unplugin-vue-i18n/messages';
import router from './router';

const app = createApp(App);
const i18n = createI18n({
  availableLocales: ['es', 'en'],
  fallbackLocale: import.meta.env.VITE_SITE_LOCALE_FALLBACK || 'en',
  globalInjection: true,
  legacy: false,
  locale: import.meta.env.VITE_SITE_LOCALE || 'es',
  messages,
});

// Components
app
  .component('AccountLoginForm', AccountLoginForm)
  .component('CasHeader', CasHeader)
  .component('CasFooter', CasFooter)
  .component('CasAlert', CasAlert)
  .component('CategoryForm', CategoryForm)
  .component('CategoryQuirkForm', CategoryQuirkForm)
  .component('GroupForm', GroupForm)
  .component('UsersDashboardListItem', UsersDashboardListItem)
  .component('TasksListGroupItem', TasksListGroupItem)
  .component('ExecutionsListGroupItem', ExecutionsListGroupItem);

app.use(createPinia().use(createORM()));
app.use(router);

library.add(
  faAngleDown,
  faAngleUp,
  faCaretRight,
  faCircleXmark,
  faDiagramProject,
  faFileCircleQuestion,
  faMinus,
  faPenToSquare,
  faPencil,
  faPlus,
  faRightToBracket,
  faSearch,
  faSliders,
);

app.component('FontAwesomeIcon', FontAwesomeIcon);

app.use(i18n);
app.mount('#app');
