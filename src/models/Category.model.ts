import { HasMany, Str } from 'pinia-orm/dist/decorators';
import { Model } from 'pinia-orm';
import Quirk from '@/models/Quirk.model';

export default class Category extends Model {
  static entity = 'categories';

  static primaryKey = 'codename';

  @Str()
  declare codename: string;

  @Str()
  declare name: string;

  @HasMany(() => Quirk, 'categoryCodename')
  declare quirks: Quirks[];
}
