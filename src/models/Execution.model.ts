import { DateCast } from 'pinia-orm/casts';
import { Model } from 'pinia-orm';
import Task from '@/models/Task.model';

export default class Execution extends Model {
  static entity = 'executions';

  static primaryKey = 'id';

  static fields() {
    return {
      description: this.string(''),
      finishedAt: this.attr(null),
      id: this.string(''),
      name: this.string(''),
      startedAt: this.attr(null),
      status: this.string(''),
      summary: this.string(''),
      tasks: this.hasMany(Task, 'executionId'),
    };
  }

  static casts() {
    return {
      finishedAt: DateCast,
      startedAt: DateCast,
    };
  }
}
