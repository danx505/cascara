import { BelongsToMany, Str } from 'pinia-orm/dist/decorators';
import GroupAssignedTask from '@/models/GroupAssignedTask.model';
import GroupSolvedTask from '@/models/GroupSolvedTask.model';
import GroupUser from '@/models/GroupUser.model';
import { Model } from 'pinia-orm';
import Task from '@/models/Task.model';
import User from '@/models/User.model';

export default class Group extends Model {
  static entity = 'groups';

  static primaryKey = 'codename';

  @Str()
  declare codename: string;

  @Str()
  declare name: string;

  @BelongsToMany(() => User, () => GroupUser, 'groupCodename', 'userUsername')
  declare users: User[];

  @BelongsToMany(() => Task, () => GroupAssignedTask, 'groupCodename', 'taskId')
  declare assignedTasks: Task[];

  @BelongsToMany(() => Task, () => GroupSolvedTask, 'groupCodename', 'taskId')
  declare solvedTasks: Task[];
}
