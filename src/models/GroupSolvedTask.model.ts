import { Model } from 'pinia-orm';
import { Str } from 'pinia-orm/dist/decorators';

export default class GroupSolvedTask extends Model {
  static entity = 'groupSolvedTasks';

  static primaryKey = ['groupCodename', 'taskId'];

  @Str()
  declare groupCodename: string;

  @Str()
  declare taskId: string;
}
