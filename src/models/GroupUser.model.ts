import { Model } from 'pinia-orm';
import { Str } from 'pinia-orm/dist/decorators';

export default class GroupUser extends Model {
  static entity = 'groupUsers';

  static primaryKey = ['groupCodename', 'userUsername'];

  @Str()
  declare groupCodename: string;

  @Str()
  declare userUsername: string;
}
