import { BelongsTo, BelongsToMany, Str } from 'pinia-orm/dist/decorators';
import Category from '@/models/Category.model';
import { Model } from 'pinia-orm';
import QuirkUser from '@/models/QuirkUser.model';
import User from '@/models/User.model';

export default class Quirk extends Model {
  static entity = 'quirks';

  static primaryKey = ['categoryCodename', 'codename'];

  @Str()
  declare codename: string;

  @Str()
  declare name: string;

  @Str()
  declare categoryCodename: string;

  @BelongsTo(() => Category, 'categoryCodename', 'codename')
  declare category: Category;

  @Str()
  declare pk: string;

  @BelongsToMany(() => User, () => QuirkUser, 'quirkPk', 'userUsername', 'pk', 'username')
  declare users: User[];
}
