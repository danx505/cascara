import { Model } from 'pinia-orm';
import { Str } from 'pinia-orm/dist/decorators';

export default class QuirkUser extends Model {
  static entity = 'quirkUsers';

  static primaryKey = ['quirkPk', 'userUsername'];

  @Str()
  declare quirkPk: string;

  @Str()
  declare userUsername: string;
}
