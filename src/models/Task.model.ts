import { DateCast } from 'pinia-orm/casts';
import Execution from '@/models/Execution.model';
import Group from '@/models/Group.model';
import GroupAssignedTask from '@/models/GroupAssignedTask.model';
import GroupSolvedTask from '@/models/GroupSolvedTask.model';
import { Model } from 'pinia-orm';
import User from '@/models/User.model';
import UserAssignedTask from '@/models/UserAssignedTask.model';
import UserSolvedTask from '@/models/UserSolvedTask.model';

export default class Task extends Model {
  static entity = 'tasks';

  static primaryKey = 'id';

  static fields() {
    return {
      actors: this.belongsToMany(User, UserSolvedTask, 'taskId', 'userUsername'),
      candidates: this.belongsToMany(User, UserAssignedTask, 'taskId', 'userUsername'),
      description: this.string(''),
      execution: this.belongsTo(Execution, 'executionId'),
      executionId: this.attr(null),
      finishedAt: this.attr(null),
      groupActors: this.belongsToMany(Group, GroupSolvedTask, 'taskId', 'groupCodename'),
      groupCandidadtes: this.belongsToMany(Group, GroupAssignedTask, 'taskId', 'groupCodename'),
      id: this.string(''),
      name: this.string(''),
      startedAt: this.attr(null),
      status: this.string(''),
    };
  }

  static casts() {
    return {
      finishedAt: DateCast,
      startedAt: DateCast,
    };
  }
}
