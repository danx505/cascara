import { BelongsToMany, Bool, Str } from 'pinia-orm/dist/decorators';
import Group from '@/models/Group.model';
import GroupUser from '@/models/GroupUser.model';
import { Model } from 'pinia-orm';
import Quirk from '@/models/Quirk.model';
import QuirkUser from '@/models/QuirkUser.model';
import Task from '@/models/Task.model';
import UserAssignedTask from '@/models/UserAssignedTask.model';
import UserSolvedTask from '@/models/UserSolvedTask.model';

export default class User extends Model {
  static entity = 'users';

  static primaryKey = 'username';

  @Str()
  declare username: string;

  @Str()
  declare fullname: string;

  @Str()
  declare email: string;

  @Bool(false)
  declare isStaff: boolean;

  @Bool(false)
  declare isSuperuser: boolean;

  @Bool(false)
  declare isAdmin: boolean;

  @BelongsToMany(() => Group, () => GroupUser, 'userUsername', 'groupCodename')
  declare groups: Group[];

  @BelongsToMany(() => Task, () => UserAssignedTask, 'userUsername', 'taskId')
  declare assignedTasks: Task[];

  @BelongsToMany(() => Task, () => UserSolvedTask, 'userUsername', 'taskId')
  declare solvedTasks: Task[];

  @BelongsToMany(() => Quirk, () => QuirkUser, 'userUsername', 'quirkPk', 'username', 'pk')
  declare quirks: Quirk[];
}
