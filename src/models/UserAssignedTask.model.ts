import { Model } from 'pinia-orm';
import { Str } from 'pinia-orm/dist/decorators';

export default class UserAssignedTask extends Model {
  static entity = 'userAssignedTasks';

  static primaryKey = ['userUsername', 'taskId'];

  @Str()
  declare userUsername: string;

  @Str()
  declare taskId: string;
}
