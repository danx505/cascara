import { Model } from 'pinia-orm';
import { Str } from 'pinia-orm/dist/decorators';

export default class UserSolvedTask extends Model {
  static entity = 'userSolvedTasks';

  static primaryKey = ['userUsername', 'taskId'];

  @Str()
  declare userUsername: string;

  @Str()
  declare taskId: string;
}
