export default function authHeader() {
  const user = JSON.parse(localStorage.getItem('user'));

  if (user) {
    const raw = `${user.username}:${user.token}`;
    let auth = '';

    if (typeof window === 'undefined') {
      auth = new Buffer(raw).toString('base64');
    } else {
      auth = btoa(raw);
    }

    return { Authorization: `Basic ${auth}` };
  }
  return {};
}
