import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;

const login = (user) => {
  return axios
    .post(`${API_URL}/auth/signin/impersonate`, {
      password: user.password,
      username: user.username,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem('user', JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem('user');
};

export default {
  login,
  logout,
};
