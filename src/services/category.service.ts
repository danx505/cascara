import authHeader from './auth-header';
import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;

const getCategory = (codename) => {
  const cleanCodename = encodeURIComponent(codename);
  return axios.get(`${API_URL}/categories/${cleanCodename}`, {
    headers: authHeader(),
  });
};

const getCategoryQuirks = (codename, { searchQuery = null } = {}) => {
  const cleanCodename = encodeURIComponent(codename);
  const params = {
    limit: 1000,
    ...(searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }),
  };

  return axios.get(`${API_URL}/categories/${cleanCodename}/quirks`, {
    headers: authHeader(),
    params,
  });
};

const getCategories = () => {
  return axios.get(`${API_URL}/categories`, { headers: authHeader() });
};

const postCategory = (payload) => {
  return axios.post(
    `${API_URL}/categories`,
    {
      codename: payload.codename,
      name: payload.name,
      quirks: [],
    },
    {
      headers: authHeader(),
    },
  );
};

const postCategoryQuirk = (codename, payload) => {
  const cleanCodename = encodeURIComponent(codename);

  return axios.post(
    `${API_URL}/categories/${cleanCodename}/quirks`,
    {
      codename: payload.codename,
      name: payload.name,
    },
    {
      headers: authHeader(),
    },
  );
};

export default {
  getCategories,
  getCategory,
  getCategoryQuirks,
  postCategory,
  postCategoryQuirk,
};
