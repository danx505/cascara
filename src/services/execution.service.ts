import authHeader from './auth-header';
import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;
const DEFAULT_ITEM_LIMIT = 50;

const getExecution = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return axios.get(`${API_URL}/executions/${cleanId}`, {
    headers: authHeader(),
  });
};

const getExecutionSummary = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return axios.get(`${API_URL}/execution/${cleanId}/summary`, {
    headers: authHeader(),
  });
};

const getExecutionTasks = (id: string) => {
  const params = {
    // eslint-disable-next-line camelcase
    execution__id__eq: id,
    limit: DEFAULT_ITEM_LIMIT,
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

const getExecutions = ({
  limit = DEFAULT_ITEM_LIMIT,
  searchQuery = null,
  startedAtDateGe = null,
  startedAtDateLe = null,
  startedAtGt = null,
  startedAtLt = null,
  statusIn = null,
  status = null,
} = {}) => {
  const params = {
    limit,
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
    ...((startedAtDateGe && {
      // eslint-disable-next-line camelcase
      started_at__date__ge: startedAtDateGe,
    }) as unknown as object),
    ...((startedAtDateLe && {
      // eslint-disable-next-line camelcase
      started_at__date__le: startedAtDateLe,
    }) as unknown as object),
    ...((startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }) as unknown as object),
    ...((startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }) as unknown as object),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...((status &&
      ['ongoing', 'finished', 'cancelled'].includes(status) && {
        // eslint-disable-next-line camelcase
        status__eq: status,
      }) as unknown as object),
  };

  return axios.get(`${API_URL}/executions`, {
    headers: authHeader(),
    params,
  });
};

export default {
  getExecution,
  getExecutionSummary,
  getExecutionTasks,
  getExecutions,
};
