import authHeader from './auth-header';
import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;

const getGroup = (codename) => {
  const cleanCodename = encodeURIComponent(codename);
  return axios.get(`${API_URL}/groups/${cleanCodename}`, {
    headers: authHeader(),
  });
};

const getGroupUsers = (codename) => {
  const params = {
    // eslint-disable-next-line camelcase
    groups__codename__eq: codename,
    limit: 1000,
  };

  return axios.get(`${API_URL}/users`, {
    headers: authHeader(),
    params,
  });
};

const getGroupAssignedTasks = (
  codename,
  {
    searchQuery = null,
    startedAtGt = null,
    startedAtLt = null,
    statusIn = null,
    status = null,
  } = {},
) => {
  const params = {
    // eslint-disable-next-line camelcase
    candidates__groups__codename__eq: codename,
    limit: 50,
    ...(searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }),
    ...(startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }),
    ...(startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...(status && {
      // eslint-disable-next-line camelcase
      status__in: status,
    }),
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

const getGroupSolvedTasks = (
  codename,
  {
    searchQuery = null,
    startedAtGt = null,
    startedAtLt = null,
    statusIn = null,
    status = null,
  } = {},
) => {
  const params = {
    // eslint-disable-next-line camelcase
    actors__groups__codename__eq: codename,
    limit: 50,
    ...(searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }),
    ...(startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }),
    ...(startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...(status && {
      // eslint-disable-next-line camelcase
      status__in: status,
    }),
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

const getGroups = () => {
  const params = {
    limit: 1000,
  };

  return axios.get(`${API_URL}/groups`, { headers: authHeader(), params });
};

const postGroup = (payload) => {
  return axios.post(
    `${API_URL}/groups`,
    {
      codename: payload.codename,
      name: payload.name,
      quirks: [],
    },
    {
      headers: authHeader(),
    },
  );
};

export default {
  getGroup,
  getGroupAssignedTasks,
  getGroupSolvedTasks,
  getGroupUsers,
  getGroups,
  postGroup,
};
