import authHeader from './auth-header';
import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;
const DEFAULT_ITEM_LIMIT = 50;

const getTasks = ({
  actorsGroupsCodenameIn = null,
  candidatesGroupsCodenameIn = null,
  searchQuery = null,
  startedAtGt = null,
  startedAtLt = null,
  statusIn = null,
  status = null,
} = {}) => {
  const params = {
    limit: DEFAULT_ITEM_LIMIT,
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
    ...((startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }) as unknown as object),
    ...((startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }) as unknown as object),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...((status &&
      ['ongoing', 'finished', 'cancelled'].includes(status) && {
        // eslint-disable-next-line camelcase
        status__eq: status,
      }) as unknown as object),
    ...(Array.isArray(actorsGroupsCodenameIn) && {
      // eslint-disable-next-line camelcase
      actors__groups__codename__in: String(actorsGroupsCodenameIn),
    }),
    ...(Array.isArray(candidatesGroupsCodenameIn) && {
      // eslint-disable-next-line camelcase
      candidates__groups__codename__in: String(candidatesGroupsCodenameIn),
    }),
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

export default {
  getTasks,
};
