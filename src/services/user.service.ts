import authHeader from './auth-header';
import axios from 'axios';

const API_URL = import.meta.env.VITE_PVM_API_URL;

const getUser = (username) => {
  const cleanUsername = encodeURIComponent(username);
  return axios.get(`${API_URL}/users/${cleanUsername}`, {
    headers: authHeader(),
  });
};

const getUserAssignedTasks = (
  username,
  {
    searchQuery = null,
    startedAtGt = null,
    startedAtLt = null,
    statusIn = null,
    status = null,
  } = {},
) => {
  const params = {
    // eslint-disable-next-line camelcase
    candidates__identifier__eq: username,
    limit: 50,
    ...(searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }),
    ...(startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }),
    ...(startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...(status && {
      // eslint-disable-next-line camelcase
      status__in: status,
    }),
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

const getUserSolvedTasks = (
  username,
  {
    searchQuery = null,
    startedAtGt = null,
    startedAtLt = null,
    statusIn = null,
    status = null,
  } = {},
) => {
  const params = {
    // eslint-disable-next-line camelcase
    actors__identifier__eq: username,
    limit: 50,
    ...(searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }),
    ...(startedAtGt && {
      // eslint-disable-next-line camelcase
      started_at__gt: startedAtGt,
    }),
    ...(startedAtLt && {
      // eslint-disable-next-line camelcase
      started_at__lt: startedAtLt,
    }),
    ...(Array.isArray(statusIn) && {
      // eslint-disable-next-line camelcase
      status__in: String(statusIn),
    }),
    ...(status && {
      // eslint-disable-next-line camelcase
      status__in: status,
    }),
  };

  return axios.get(`${API_URL}/pointers`, {
    headers: authHeader(),
    params,
  });
};

const getUserGroups = (username) => {
  const params = {
    limit: 50,
    // eslint-disable-next-line camelcase
    users__identifier__eq: username,
  };

  return axios.get(`${API_URL}/groups`, {
    headers: authHeader(),
    params,
  });
};

const getUserQuirks = (username, codename) => {
  const cleanUsername = encodeURIComponent(username);
  const cleanCodename = encodeURIComponent(codename);
  const params = {
    limit: 50,
  };

  return axios.get(`${API_URL}/users/${cleanUsername}/${cleanCodename}`, {
    headers: authHeader(),
    params,
  });
};

const getUsers = () => {
  const params = {
    limit: 1000,
  };

  return axios.get(`${API_URL}/users`, { headers: authHeader(), params });
};

export default {
  getUser,
  getUserAssignedTasks,
  getUserGroups,
  getUserQuirks,
  getUserSolvedTasks,
  getUsers,
};
