import { defineStore } from 'pinia';

const useAlertStore = defineStore({
  actions: {
    clear() {
      this.alert = null;
    },
    error(message) {
      this.alert = { message, type: 'alert-danger' };
    },
    success(message) {
      this.alert = { message, type: 'alert-success' };
    },
  },

  id: 'alert',

  state: () => ({
    alert: null,
  }),
});

export { useAlertStore };
