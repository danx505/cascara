import axios from 'axios';
import { defineStore } from 'pinia';
import router from '@/router';
import { useAlertStore } from '@/stores/alert.store';

const baseUrl = `${import.meta.env.VITE_PVM_API_URL}/auth`;

const useAuthStore = defineStore({
  actions: {
    async fetchProviders() {
      try {
        const { data } = await axios.get(`${baseUrl}/providers`);

        this.providers = data.items.map((item) => ({
          label: item.label,
          value: item.key,
        }));
      } catch (error) {
        const alertStore = useAlertStore();
        alertStore.error('request.error');
      }
    },

    async login(username, password, provider) {
      try {
        const { data } = await axios.post(`${baseUrl}/signin/${provider}`, {
          password,
          username,
        });

        this.user = {
          fullname: data.data.fullname,
          token: data.data.token,
          username: data.data.identifier,
        };

        localStorage.setItem(
          'user',
          JSON.stringify({
            fullname: data.data.fullname,
            token: data.data.token,
            username: data.data.identifier,
          }),
        );

        router.push(this.returnUrl || '/');
      } catch (error) {
        const message = 'validation.invalid';
        const statusCodeUnauthorized = 401;

        if (
          error.response?.status === statusCodeUnauthorized &&
          Array.isArray(error.response.data?.errors)
        ) {
          const data = error.response.data.errors.find((err) => err.code);
          useAlertStore().error(data?.code || message);
        }
      }
    },

    logout() {
      this.user = null;
      localStorage.removeItem('user');
      router.push('/account/login');
    },
  },

  id: 'auth',

  state: () => ({
    providers: [],
    returnUrl: null,
    user: JSON.parse(localStorage.getItem('user')),
  }),
});

export { useAuthStore };
