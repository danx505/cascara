import Category from '@/models/Category.model';
import CategoryService from '@/services/category.service';
import { Repository } from 'pinia-orm';
import router from '@/router';
import { useAlertStore } from '@/stores/alert.store';

class CategoryRepository extends Repository {
  use = Category;

  async fetchAll() {
    try {
      const { data } = await CategoryService.getCategories();

      this.save(
        data.items.map((item) => ({
          ...item,
          codename: item.codename,
        })),
      );
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchCategory(codename, payload) {
    try {
      const { data } = await CategoryService.getCategory(codename, payload);

      this.save({
        ...data,
        codename: data.codename,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchCategoryQuirks(codename) {
    try {
      const { data } = await CategoryService.getCategoryQuirks(codename);

      this.save({
        codename,
        quirks: data.items.map((item) => ({
          ...item,
          categoryCodename: codename,
          codename: item.codename,
        })),
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async pushCategory(payload) {
    try {
      await CategoryService.postCategory(payload);
      router.push({ name: 'categories-dashboard' });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async pushCategoryQuirk(codename, payload) {
    try {
      await CategoryService.postCategoryQuirk(codename, payload);
      router.push({ name: 'categories-item-quirks', params: { codename } });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }
}

export { CategoryRepository };
