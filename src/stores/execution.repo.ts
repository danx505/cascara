import Execution from '@/models/Execution.model';
import ExecutionService from '@/services/execution.service';
import { Repository } from 'pinia-orm';
import { useAlertStore } from '@/stores/alert.store';

class ExecutionRepository extends Repository {
  use = Execution;

  async fetchAll(payload) {
    try {
      const { data } = await ExecutionService.getExecutions(payload);

      this.save(
        data.items.map((item) => ({
          ...item,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
      );
    } catch (error) {
      const alertStore = useAlertStore();
      const statusCodeForbidden = 403;
      const statusCodeUnauthorized = 401;
      if (error.request.status === statusCodeUnauthorized) {
        alertStore.error('request.forbidden');
      } else if (error.request.status === statusCodeForbidden) {
        alertStore.error('validation.invalid');
      } else {
        alertStore.error('request.error');
      }
    }
  }

  async fetchExecution(id) {
    try {
      const { data } = await ExecutionService.getExecution(id);

      this.save({
        ...data,
        finishedAt: data.finished_at,
        id: data.id,
        startedAt: data.started_at,
        status: data.status,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      const statusCodeForbidden = 403;
      const statusCodeUnauthorized = 401;
      if (error.request.status === statusCodeUnauthorized) {
        alertStore.error('request.forbidden');
      } else if (error.request.status === statusCodeForbidden) {
        alertStore.error('validation.invalid');
      } else {
        alertStore.error('request.error');
      }
    }
  }

  async fetchExecutionTasks(id) {
    try {
      const { data } = await ExecutionService.getExecutionTasks(id);

      this.save({
        id,
        tasks: data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchExecutionSummary(id) {
    try {
      const { data } = await ExecutionService.getExecutionSummary(id);

      this.save({
        id,
        summary: data,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      const statusCodeForbidden = 403;
      const statusCodeUnauthorized = 401;
      if (error.request.status === statusCodeUnauthorized) {
        alertStore.error('request.forbidden');
      } else if (error.request.status === statusCodeForbidden) {
        alertStore.error('validation.invalid');
      } else {
        alertStore.error('request.error');
      }
    }
  }
}

export { ExecutionRepository };
