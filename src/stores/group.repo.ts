import Group from '@/models/Group.model';
import GroupService from '@/services/group.service';
import { Repository } from 'pinia-orm';
import router from '@/router';
import { useAlertStore } from '@/stores/alert.store';

class GroupRepository extends Repository {
  use = Group;

  async fetchAll() {
    try {
      const { data } = await GroupService.getGroups();

      this.save(
        data.items.map((item) => ({
          ...item,
          codename: item.codename,
        })),
      );
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchGroup(codename) {
    try {
      const { data } = await GroupService.getGroup(codename);

      this.save({
        ...data,
        codename: data.codename,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchGroupUsers(codename) {
    try {
      const { data } = await GroupService.getGroupUsers(codename);

      this.save({
        codename,
        users: data.items.map((item) => ({
          ...item,
          username: item.identifier,
        })),
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchGroupAssignedTasks(codename, payload) {
    try {
      const { data } = await GroupService.getGroupAssignedTasks(codename, payload);

      this.save({
        assignedTasks: data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
        codename,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchGroupSolvedTasks(codename, payload) {
    try {
      const { data } = await GroupService.getGroupSolvedTasks(codename, payload);

      this.save({
        codename,
        solvedTasks: data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async pushGroup(payload) {
    try {
      await GroupService.postGroup(payload);
      router.push({ name: 'groups-dashboard' });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }
}

export { GroupRepository };
