import Quirk from '@/models/Quirk.model';
import { Repository } from 'pinia-orm';

class QuirkRepository extends Repository {
  use = Quirk;
}

export { QuirkRepository };
