import { Repository } from 'pinia-orm';
import Task from '@/models/Task.model';
import TaskService from '@/services/task.service';
import { useAlertStore } from '@/stores/alert.store';

class TaskRepository extends Repository {
  use = Task;

  async fetchAll(payload) {
    try {
      const { data } = await TaskService.getTasks(payload);

      this.save(
        data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
      );
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }
}

export { TaskRepository };
