import { Repository } from 'pinia-orm';
import User from '@/models/User.model';
import UserService from '@/services/user.service';
import { useAlertStore } from '@/stores/alert.store';

class UserRepository extends Repository {
  use = User;

  async fetchAll() {
    try {
      const { data } = await UserService.getUsers();

      this.save(
        data.items.map((item) => ({
          ...item,
          isAdmin: item.is_admin,
          isStaff: item.is_staff,
          isSuperuser: item.is_superuser,
          username: item.identifier,
        })),
      );
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchUser(username) {
    try {
      const { data } = await UserService.getUser(username);

      this.save({
        ...data,
        isAdmin: data.is_admin,
        isStaff: data.is_staff,
        isSuperuser: data.is_superuser,
        username: data.identifier,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchUserAssignedTasks(username, payload) {
    try {
      const { data } = await UserService.getUserAssignedTasks(username, payload);

      this.save({
        assignedTasks: data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
        username,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchUserSolvedTasks(username, payload) {
    try {
      const { data } = await UserService.getUserSolvedTasks(username, payload);

      this.save({
        solvedTasks: data.items.map((item) => ({
          ...item,
          executionId: item.execution_id,
          finishedAt: item.finished_at,
          id: item.id,
          startedAt: item.started_at,
          status: item.status,
        })),
        username,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchUserGroups(username) {
    try {
      const { data } = await UserService.getUserGroups(username);

      this.save({
        groups: data.items.map((item) => ({
          ...item,
          codename: item.codename,
        })),
        username,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }

  async fetchUserQuirks(username, categoryCodename) {
    try {
      const { data } = await UserService.getUserQuirks(username, categoryCodename);

      this.save({
        quirks: data.items.map((item) => ({
          categoryCodename,
          codename: item.codename,
          name: item.name,
          pk: `${categoryCodename}.${item.codename}`,
        })),
        username,
      });
    } catch (error) {
      const alertStore = useAlertStore();
      alertStore.error('request.forbidden');
    }
  }
}

export { UserRepository };
